package com.example.pasto

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        val btn_login:Button = findViewById(R.id.btn_login)
        btn_login.setOnClickListener {
            val intent:Intent = Intent(this,report_operation:: class.java)
            startActivity(intent)

            Toast.makeText(this,"Se presionooooo",Toast.LENGTH_LONG).show()
        }
    }
}